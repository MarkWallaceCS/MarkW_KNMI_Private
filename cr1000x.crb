'CR1000X
' *****************************************************************************
' *    __  __   _    _   _____       ____   ____   _   __   __   __  __  __   *
' *   |  \/  | | |  | | |_   _|     |  __| |  __| | | /  \ /  \ /  \ \ \/ /   *
' *   | |\/| | | |/\| |  _| |_  [ ] | |__  | |    | | |[]| |[]| |[]|  )  (    *
' *   |_|  |_| |__/\__| |_____|     |____| |_|    |_| \__/ \__/ \__/ /_/\_\   *
' *                                                                           *
' *****************************************************************************
' *   (c) Campbell Scientific Inc. 2023; KNMI 2023                            *
' *       Koninklijk Nederlands Meteorologisch Instituut                      *
' *       Modernisering Waarneem Infrastructuur - MWI                         *
' *****************************************************************************
' *   MODULE       : cr1000x.crb                                              *
' *   DESCRIPTION  : This program file handles all measurements dealing with  *
' *                : the cr1000x datalogger, and measurements used by other   *
' *                : include files                                            *
' *   VERSION      : MWI interim solution with SIAM/MUF output                *
' *   VERSION NOTES: NA                                                       *
' *   CONTACT      : Sensor Interface Team (SIT) - sit@knmi.nl                *
' *   DEVELOPED BY : Campbell Scientific Inc. and KNMI                        *
' *****************************************************************************

#If SECTION = "Section_CR1000X_Version" Then
  Dim sig_cr1000x As Long
#EndIf

#If SECTION = "Section_CR1000X_Declarations" Then
  Dim volt_file_action As Long = -2 ' Find file
  Dim volt_os_name As String * 50
  Dim scancounter_slowseq1 As Long
  Dim prev_system_scan_str As String *40
  Dim system_scan_timestamp As Long
  Dim system_scan_str As String *40
  Dim skipped_scans(6) As Long, skipped_scan_rain As Boolean
  Alias skipped_scans(1) = skipped_main_scans
  Alias skipped_scans(2) = skipped_system_scans
  Alias skipped_scans(3) = skipped_slowseq1_scans
  Alias skipped_scans(4) = skipped_slowseq2_scans
  Alias skipped_scans(5) = skipped_slowseq3_scans
  Alias skipped_scans(6) = skipped_scan_prev
  Dim calibration_errors_long As Long
  Dim cr1000x_dgc_float
  #If SOIL_TEMP Then ' Volt108 module is only connected if station is running soil temperatures.
    Dim volt108_dgc_float(2) As Float
    Dim volt108_supply_volts As Float
  #EndIf
  Dim battery_volts_float As Float
  Dim board_status_str As String *1 = "A" ' Initialize to "A" datalogger on startup.  This is used to as status code to indicate datalogger has restarted.
  Public force_cal_error As Boolean, force_temp_error As Boolean
  'Dim cr1k_tf_status As Long, cr1k_tf_name As String *48 ' Table file report variables.
#EndIf



#If SECTION = "Section_CR1000X_Storage" Then

  DataTable(diagnostics, True, -1)
    CardOut(0, CARD_OUT_DAYS * 7200 )
    DataTime(1)
    Sample(1, system_scan_timestamp, nSec) : FieldNames("last_system_scan")
    Sample(1, skipped_system_scans, Long)
    Sample(1, skipped_main_scans, Long)
    Sample(1, skipped_slowseq1_scans, Long)
    Sample(1, skipped_slowseq2_scans, Long)
    Sample(1, skipped_slowseq3_scans, Long)
    Sample(1, board_status_str, String)
    Sample(1, Status.WatchdogErrors, Long) : FieldNames("CR1000x_watchdogerror_cnt")
    Sample(1, calibration_errors_long, Long)
    Sample(1, cr1000x_dgc_float, FP2)
    #If SOIL_TEMP Then ' Volt108 module is only connected if station is running soil temperatures.
      Sample(1, volt108_dgc_float(1), FP2)
      Sample(1, volt108_dgc_float(2), FP2)
      Sample(1, CPIStatus.FrameErr, Long) : FieldNames("volt108_frame_errors")
      Sample(1, volt108_supply_volts, FP2)
    #EndIf
    Sample(1, battery_volts_float, FP2)
    #If MQTT_PUBLISH_ENABLE Then
      MQTTPublishTable(1, 1, 0, 0, 2, LONGITUDE, LATITUDE, ELEVATION) ' QoS=1, OutputFormat=2 is GEOJSON.
    #EndIf
  EndTable

#EndIf

#If SECTION = "Section_CR1000X_Maint_Switches" Then

#EndIf ' "Section_CR1000X_Maintenance_Flag"


#If SECTION = "Section_CR1000X_Measurement" Then
  scancounter_slowseq1 += 1
  system_scan_str = Status.LastSystemScan
  system_scan_timestamp = Status.LastSystemScan
  If system_scan_str = prev_system_scan_str Then
    If scancounter_slowseq1 > 50 AND Public.TimeStamp - system_scan_timestamp > 600 Then
      calibration_errors_long += 1
    EndIf
  Else
    calibration_errors_long = Status.ErrorCalib
  EndIf

  prev_system_scan_str = system_scan_str

  '\ Power supply and panel temperature measurements on CR1000X and Volt108:
  Battery(battery_volts_float)
  PanelTemp(cr1000x_dgc_float, FNOTCH)
  #If SOIL_TEMP Then ' Volt108 module is only connected if stations with soil temperatures.
    CDM_Battery(CDMTYPE, 1, volt108_supply_volts)
    CDM_PanelTemp(CDMTYPE, 1, volt108_dgc_float(1), 1, 1, FNOTCH)
    CDM_PanelTemp(CDMTYPE, 1, volt108_dgc_float(2), 1, 2, FNOTCH)
  #EndIf
  'Measure the three physical maintence switches
  ''SE13 -> maint_switch_phys_float(1) -> maint_switch_ceilometer_phys
  ''SE14 -> maint_switch_phys_float(2) -> maint_switch_visibility_phys
  ''SE15 -> maint_switch_phys_float(3) -> maint_switch_solar_rad_phys
  'This measurment is setup to measure physical switches fast.
  ''voltse must use a range code with the apended'C' [mv1000*C*]
  ''If NAN the switch is open. [out of maintenance mode]
  ''If not NAN the switch is closed [in maintenance mode]
  VoltSe (maint_switch_phys_float(),3,mV1000C,13,False,20,30000,1.0,0)
  'convert float value to boolean to be consistant with other maintenance switches
  For i_maint_switch_phys = 1 To 3
    If maint_switch_phys_float(i_maint_switch_phys) = NAN Then
      maint_switch_phys_bool(i_maint_switch_phys) = false
    Else
      maint_switch_phys_bool(i_maint_switch_phys) = true
    EndIf
  Next i_maint_switch_phys
#EndIf



#If SECTION = "Section_MastSwitch_Measurement"  ' This runs from SlowSequenceScan#1.

  #If WIND_SPEED = True OR WIND_DIRECTION = True OR PRESSURE = True Then

    Dim mast_switch_mamps
    CurrentSE(mast_switch_mamps, 1,mV1000, RG1, False, 0, 1000, 1.0, 0.0)
    If mast_switch_mamps > 30 Then
      mast_switch = True
    Else
      mast_switch = False
    EndIf

  #EndIf

#EndIf



#If SECTION = "Section_CR1000X_Processing" Then

  board_status_str = "0"
  If scancounter_slowseq1 = 1 Then
    board_status_str = "A" ' Datalogger has is starting up or has restarted.
  ElseIf calibration_errors_long <> 0 OR force_cal_error Then
    board_status_str = "X"
  ElseIf cr1000x_dgc_float > 55 OR force_temp_error Then
    board_status_str = "x"
    #If SOIL_TEMP Then
    ElseIf volt108_dgc_float(1) > 55 OR volt108_dgc_float(2) > 55 Then
      board_status_str = "x"
    #EndIf
  EndIf

#EndIf



#If SECTION = "Section_CR1000X_Save" Then

  CallTable diagnostics

#EndIf



'}
