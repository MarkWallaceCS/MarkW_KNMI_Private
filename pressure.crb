'CR1000X
' *****************************************************************************
' *    __  __   _    _   _____       ____   ____   _   __   __   __  __  __   *
' *   |  \/  | | |  | | |_   _|     |  __| |  __| | | /  \ /  \ /  \ \ \/ /   *
' *   | |\/| | | |/\| |  _| |_  [ ] | |__  | |    | | |[]| |[]| |[]|  )  (    *
' *   |_|  |_| |__/\__| |_____|     |____| |_|    |_| \__/ \__/ \__/ /_/\_\   *
' *                                                                           *
' *****************************************************************************
' *   (c) Campbell Scientific Inc. 2023; KNMI 2023                            *
' *       Koninklijk Nederlands Meteorologisch Instituut                      *
' *       Modernisering Waarneem Infrastructuur - MWI                         *
' *****************************************************************************
' *   MODULE       : pressure.crb                                             *
' *   DESCRIPTION  : CR1000X module for measuring/processing Paroscientific   *
' *                  Digiquartz 1015A pressure sensor.                        *
' *   VERSION      : MWI interim solution with SIAM/MUF output                *
' *   VERSION NOTES: NA                                                       *
' *   CONTACT      : Sensor Interface Team (SIT) - sit@knmi.nl                *
' *   DEVELOPED BY : Campbell Scientific Inc. and KNMI                        *
' *****************************************************************************
#If SECTION = "Section_PRESSURE_Version" Then
  Dim sig_pressure As Long
#EndIf  ' Section_Pressure_Version

#If (PRESSURE = True) Then
  #If SECTION = "Section_PRESSURE_Declarations" Then
    ' Values needed for measuring Press sensir
    StructureType PressStructure
      version As String  ' Device firmware version
      id As Long  ' Device ID
      unit_check As Long  ' Units
      pressure_setting As Long  ' Pressure setting
      temp_resolution As Long  ' Temperature resolution
      pressure As Float  ' Pressure measurement
      temp As Float  ' Sensor Temperature
      ' Message from sensor
      response_version As String  ' Device firmware version
      response_id As String  ' Device ID
      response_units As String  ' Units
      response_press_setting As String  ' Pressure setting
      response_temp_res As String  ' Temperature resolution
      response_pressure As String  ' Pressure measurement
      response_temp As String  ' Sensor Temperature
    EndStructureType
    ' tmp variable for storing response
    Dim press_tmp(8) As String * 16
    Alias press_tmp(1) = press_tmp_vr
    Alias press_tmp(2) = press_tmp_id
    Alias press_tmp(3) = press_tmp_un
    Alias press_tmp(4) = press_tmp_pr
    Alias press_tmp(5) = press_tmp_tr
    Alias press_tmp(6) = press_tmp_p3
    Alias press_tmp(7) = press_tmp_q3
    Alias press_tmp(8) = press_tmp_vr_2
    ' Poll commands
    Dim vr_poll As String * 12 = "*9900VR" + CRLF    ' Read firmware version
    Dim id_poll As String * 12 = "*9900ID" + CRLF    ' Initialize sensor identity
    Dim un_poll As String * 12 = "*0100UN" + CRLF    ' Check units
    Dim pr_poll As String * 12 = "*0100PR" + CRLF    ' Check pressure integration time
    Dim tr_poll As String * 12 = "*0100TR" + CRLF    ' Check temperature read period
    Dim p3_poll As String * 12 = "*0100P3" + CRLF    ' Request pressure measurement
    Dim q3_poll As String * 12 = "*0100Q3" + CRLF    ' Request temperature measurement
    ' Structure variable
    #If (DEBUG_MODE = True) Then
      Public Press As PressStructure
      #If (SIAM = True) Then
        Dim press_check(10) As Boolean
      #EndIf
    #Else
      Dim Press As PressStructure
    #EndIf  ' DEBUG_MODE

    #If (SIAM = True) Then
      ' Variables needed for processing siam
      ' SIAM processed data items structure used by all the sensor.crb's:
      Dim PressPS As SiamProc
      Dim p3_history As Float
      'Dim press_highest_code As Long
      Const PRESS_N_STATUS_CODES = 14  ' ___________ = {  1,   2,   3,   4,   5,   6,   7,   8,   9,
      Dim press_status_code_string(14) As String * 1 = {"X", "A", "Z", "N", "B", "J", "E", "C", "D",
      "m", "l", "x", "1", "0"}
      '10,  11,  12,  13,  14}
      ' _uc suffix is for upper case.  _lc is for lower case. CRBasic is not case sensitive.
      Dim press_status_flag(14) As Boolean
      Alias press_status_flag( 1) = press_X_flag_uc ' CR1000X calibration error (X)
      Alias press_status_flag( 2) = press_A_flag_uc ' CR1000X reset (A)
      Alias press_status_flag( 3) = press_Z_flag_uc ' Dip switch (Z)
      Alias press_status_flag( 4) = press_N_flag_uc ' Maintenance switch (N)
      Alias press_status_flag( 5) = press_B_flag_uc ' No connection (B)
      Alias press_status_flag( 6) = press_J_flag_uc ' Data Error (J)
      Alias press_status_flag( 7) = press_E_flag_uc ' Barometer configuration error (E)
      Alias press_status_flag( 8) = press_C_flag_uc ' Jump error (C)
      Alias press_status_flag( 9) = press_D_flag_uc ' Range error (D)
      Alias press_status_flag(10) = press_m_flag_lc ' Barometer temp > 70 (m)
      Alias press_status_flag(11) = press_l_flag_lc ' Barometer temp < -30 (l)
      Alias press_status_flag(12) = press_x_flag_lc ' CR1000X panel temp > 55 (x)
      Alias press_status_flag(13) = press_1_flag_nc ' Test Mode
      Alias press_status_flag(14) = press_0_flag_nc ' Normal Mode
    #EndIf  ' SIAM
  #EndIf  ' Section_Pressure_Declarations

  #If SECTION = "Section_PRESSURE_Storage" Then
    DataTable(Press_Cloud, True, -1)
      CardOut(0, CARD_OUT_DAYS * 7200)
      DataTime(1)
      Sample(1, Press, PressStructure)
      #If (MQTT_PUBLISH_ENABLE = True) Then
        MQTTPublishTable(1, 1, 0, 0, 2, LONGITUDE, LATITUDE, ELEVATION) ' QoS=1, OutputFormat=2 is GEOJSON.
      #EndIf
    EndTable

    #If (SIAM = True) Then
      DataTable(Press_Siam,True,-1)
        DataTime(1)
        Sample(1,PressPS,SiamProc)
        FieldNames("PS")
      EndTable

      #If (DEBUG_MODE = True) Then
        DataTable(Press_Debug, True, DEBUG_N_RECS)
          ' Targeting around 50 days ring time and size on card no bigger than 500 MBytes.
          CardOut(0, 5*1440  * CARD_OUT_DAYS)
          DataTime(1)
          Sample(10,press_check,Boolean)
          Sample(14,press_status_flag,Boolean)
        EndTable
      #EndIf  ' DEBUG_MODE
    #EndIf  ' SIAM
  #EndIf  ' Section_PRESSURE_Storage


  #If SECTION = "Section_PRESSURE_Functions" Then
    #If (SIAM = True) Then
      Function Find_PRESSURE_Highest_Code() As Long
        ' Find the highest prioirty set status code
        Dim i As Long
        For i = 1 To PRESS_N_STATUS_CODES
          If (press_status_flag(i) = True) Then
            ExitFor
          EndIf
        Next i
        Return i
      EndFunction
    #EndIf ' SIAM

    Function Get_Str_Value(val_press As String * 20) As String
      ' Pull value out of message from sensor (Everything after = )
      Dim str_val As String, str_val_len
      Dim val_length As Long, val_position As Long
      val_length = Len(val_press)
      val_position = InStr (1,val_press,"=",2)
      str_val_len = val_length - val_position - 2
      str_val = Mid (val_press,val_position + 1,str_val_len)
      Return str_val
    EndFunction

    Function Get_Flt_Value(val_press As String) As Float
      ' Pull float value from sensor message
      Dim flt_val As Float
      flt_val = Mid (val_press,6,10)  'pull numbers after header
      Return flt_val
    EndFunction

    Function Get_ID(val_press As String) As String
      ' Pull ID from sensor message
      Dim id_val As String
      id_val = Right(val_press,5)
      id_val = Left(id_val,1)
      Return id_val
    EndFunction

    #If (SIAM = True) Then
      Sub Char_Check(check_val As String)
        ' Check for un-allowed characters
        Dim i_check As Long
        Dim char_str As String * 1
        Dim check_length As Long
        Dim str As String
        Erase(str)
        str = check_val
        check_length = Len(check_val) - 5
        check_val = Right (check_val,(check_length))

        For i_check = 1 To (check_length - 2)
          char_str = check_val(1, 1, i_check)
          char_str = Left(char_str,1)
          ' There is a non-number in string
          If ((ASCII(char_str) < 48) OR (ASCII(char_str) > 57)) Then
            If ((ASCII(char_str) = 43) OR (ASCII(char_str) = 45) OR (ASCII(char_str) = 46)) Then
              ' No forbidden character
            Else  ' Forbidden character found
              #If (Debug_Mode = True) Then
                press_check(10) = True
              #EndIf
              press_J_flag_uc = True
              ExitFor
            EndIf
          Else
            ' number found ok
          EndIf
        Next i_check
      EndSub
    #EndIf  ' SIAM
  #EndIf  ' Section PRESSURE Functions

  #If SECTION = "Section_PRESSURE_Measurement" Then

    ' Open port at start of every 12 second interval for SDM-SIO4A/SDM-SIO2R hot swap.
    ' See initialize.crb for com port and settings constants.
    SerialOpen(COMPORT_PRESSURE, BAUD_RATE_PRESSURE, COM_FORMAT_PRESSURE, 0, BUFFER_SIZE_PRESSURE)

    #If (SIAM = True) Then
      ' Clear all status flags before sending measurment requests
      Erase(press_status_flag())  ' Status flags
      #If (Debug_Mode = True) Then
        Erase(press_check)          ' Clear check flags when in debug mode
      #EndIf
      ' Set P3_History to measure pressure from last 12 Second Period used for checking Status C
      p3_history = Press.pressure
    #EndIf  ' SIAM
    ' Nested polls, so only send new poll after a succesful previous one
    ' Erase temp string responses from sensor go into.
    Erase(press_tmp())
    ' VR Command  Example Response *0001VR=R5.10
    SerialFlush(COMPORT_PRESSURE)
    SerialOut(COMPORT_PRESSURE, vr_poll, "", 0, 0)
    SerialIn(press_tmp_vr, COMPORT_PRESSURE, 30, CRLF, 20)
    press.response_version = press_tmp_vr
    ' Sensor echos command here
    SerialIn(press_tmp_vr_2, COMPORT_PRESSURE, 30, CRLF, 20)
    ' If received response then next poll
    If InStr(1, press_tmp_vr, "*", 2) Then
      ' ID Command  Example Response *9901ID
      SerialFlush(COMPORT_PRESSURE)
      SerialOut(COMPORT_PRESSURE, id_poll, "", 0, 0)
      SerialIn(press_tmp_id, COMPORT_PRESSURE, 30, CRLF, 20)
      press.response_id = press_tmp_id
      ' If received response then next poll
      If InStr(1,press_tmp_id,"*",2) Then
        ' UN Command  Example Response *0001UN=2
        SerialFlush (COMPORT_PRESSURE)
        SerialOut (COMPORT_PRESSURE,un_poll,"",0,0)
        SerialIn (press_tmp_un,COMPORT_PRESSURE,30,CRLF,20)
        press.response_units = press_tmp_un
        ' If received response then next poll
        If InStr(1,press_tmp_un,"*",2) Then
          ' PR Command  Example Response *0001PR=200
          SerialFlush (COMPORT_PRESSURE)
          SerialOut (COMPORT_PRESSURE,pr_poll,"",0,0)
          SerialIn (press_tmp_pr,COMPORT_PRESSURE,30,CRLF,20)
          press.response_press_setting = press_tmp_pr
          ' If received response then next poll
          If InStr(1,press_tmp_pr,"*",2) Then
            ' TR Command  Example Response *0001TR=800
            SerialFlush (COMPORT_PRESSURE)
            SerialOut (COMPORT_PRESSURE,tr_poll,"",0,0)
            SerialIn (press_tmp_tr,COMPORT_PRESSURE,800,CRLF,20)
            press.response_temp_res = press_tmp_tr
            ' If received response then next poll
            If InStr(1,press_tmp_tr,"*",2) Then
              ' P3 Command  Example Response *000114.71234 Value = 14.71234
              SerialFlush (COMPORT_PRESSURE)
              SerialOut (COMPORT_PRESSURE,p3_poll,"",0,0)
              SerialIn (press_tmp_p3,COMPORT_PRESSURE,700,CRLF,20)
              press.response_pressure = press_tmp_p3
              ' If received response then succes
              If InStr(1,press_tmp_p3,"*",2) Then
                ' Q3 Command  Example Response *000122.345 Value = 22.345
                SerialFlush (COMPORT_PRESSURE)
                SerialOut (COMPORT_PRESSURE,q3_poll,"",0,0)
                SerialIn (press_tmp_q3,COMPORT_PRESSURE,400,CRLF,20)
                press.response_temp = press_tmp_q3
                If InStr(1,press_tmp_q3,"*",2) Then
                  ' Good
                Else  ' Failed Q3 Poll
                  #If (SIAM = True) Then
                    press_B_flag_uc = True
                  #EndIf  ' SIAM
                EndIf  ' Q3
              Else  ' Failed P3 poll
                #If (SIAM = True) Then
                  press_B_flag_uc = True
                #EndIf  ' SIAM
              EndIf  ' P3
            Else    ' Failed TR poll
              #If (SIAM = True) Then
                press_B_flag_uc = True
              #EndIf  ' SIAM
            EndIf  ' TR
          Else    ' Failed PR poll
            #If (SIAM = True) Then
              press_B_flag_uc = True
            #EndIf  ' SIAM
          EndIf  ' PR
        Else    ' Failed UN poll
          #If (SIAM = True) Then
            press_B_flag_uc = True
          #EndIf  ' SIAM
        EndIf  ' UN
      Else    ' Failed ID poll
        #If (SIAM = True) Then
          press_B_flag_uc = True
        #EndIf  ' SIAM
      EndIf  ' ID
    Else    ' Failed VR poll
      #If (SIAM = True) Then
        press_B_flag_uc = True
      #EndIf  ' SIAM
    EndIf  ' VR

    ' Set values in Structure to received values
    press.version = Get_Str_Value(press_tmp_vr)
    press.id = Get_ID(press_tmp_id)
    press.unit_check = Get_Str_Value(press_tmp_un)
    press.pressure_setting = Get_Str_Value(press_tmp_pr)
    press.temp_resolution = Get_Str_Value(press_tmp_tr)
    press.pressure = Get_Flt_Value(press_tmp_p3)
    press.temp = Get_Flt_Value(press_tmp_q3)
  #EndIf  ' Section_PRESSURE_Measurement

  #If SECTION = "Section_PRESSURE_Processing" Then
    #If (SIAM = True) Then
      ' Process stuff and things here
      ' Statuses from CR1000X
      Select board_status_str
      Case "X"
        press_X_flag_uc = True
      Case "A"
        press_A_flag_uc = True
      Case "x"
        press_x_flag_lc = True
      Case Else
        press_0_flag_nc = True
      EndSelect
      ' Status Z service Switch
      If (service_switch_pressure_PS = True) Then
        press_Z_flag_uc = True
      EndIf
      ' Status N Maintenance Switch
      If ((maint_switch_pressure = True) OR (mast_switch = True) OR (maint_switch_cr1000x = True)) Then
        press_N_flag_uc = True
      EndIf
      ' Status D Range Error
      If ((press.pressure > 1060.0) OR (press.pressure < 940.0)) Then
        press_D_flag_uc = True
      EndIf
      'Status C Jump Error
      If (ABS(p3_history - press.pressure) > 15.0) Then
        press_C_flag_uc = True
      EndIf
      'Status E Barometer incorrectly Configured
      If ((press.unit_check <> 2) OR (press.pressure_setting <> 1190) OR (press.temp_resolution <> 4760)) Then
        press_E_flag_uc = True
      EndIf
      'Status J Data Error Incorrect format from sensor
      If InStr (1,press_tmp_vr,"*0001VR",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(1) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_id,"*9901ID",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(2) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_un,"*0001UN",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(3) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_pr,"*0001PR",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(4) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_tr,"*0001TR",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(5) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_p3,"*0001",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(6) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      If InStr (1,press_tmp_q3,"*0001",2) Then
      Else
        #If (Debug_Mode = True) Then
          press_check(7) = True
        #EndIf
        press_J_flag_uc = True
      EndIf
      ' Skip if J already set
      If (press_J_flag_uc = False) Then
        '  Status J String length
        If ((Len(press_tmp_p3) - 7) < 7) Then
          #If (Debug_Mode = True) Then
            press_check(8) = True
          #EndIf
          press_J_flag_uc = True
        EndIf
        If ((Len(press_tmp_q3) - 5) > 10) Then
          #If (Debug_Mode = True) Then
            press_check(9) = True
          #EndIf
          press_J_flag_uc = True
        EndIf
      EndIf
      ' Skip if J already set
      If (press_J_flag_uc = False) Then
        ' Status J Character check
        Call Char_Check(press.response_pressure)
        Call Char_Check(press.response_temp)
      EndIf
      ' Status l Sensor temp < -30C
      If (Press.temp < -30) Then
        press_l_flag_lc = True
      EndIf
      ' Status m Sensor temp > 70C
      If (Press.temp > 70) Then
        press_m_flag_lc = True
      EndIf
      ' Evaluate highest status code set for status
      PressPS.status = press_status_code_string(Find_PRESSURE_Highest_Code())
      If (InError(PressPS.status)) Then
        PressPS.siam = NaN
      Else
        PressPS.siam = Press.pressure
      EndIf
      ' Calculate running values
      AvgRun(pressPS.avg_1m, 1, pressPS.siam, 5)
      MaxRun(pressPS.max_10m, 1, pressPS.siam, 50)
      MinRun(pressPS.min_10m, 1, pressPS.siam, 50)
      AvgRun(pressPS.avg_10m, 1, pressPS.siam, 50, False, pressPS.good_samples)
      StdDevRun(pressPS.std_10m, 1, pressPS.siam, 50)
      If (pressPS.good_samples < 2) Then
        pressPS.std_10m = NaN
      EndIf
    #EndIf  ' SIAM
  #EndIf  ' Section_PRESSURE_Processing

  #If SECTION  = "Section_PRESSURE_Save" Then
    CallTable Press_Cloud
    #If (SIAM = True) Then
      CallTable Press_Siam
      #If (DEBUG_MODE = True) Then
        CallTable Press_Debug
      #EndIf
    #EndIf  ' SIAM
  #EndIf  ' Section_PRESSURE_Save
#EndIf ' PRESSURE = True
'
